# Thesis submission at Bochum

You have to be registered as a student and the semester enrollment should be done.

After completing the Thesis, inform Deans office (promotion@physik.rub.de) about your plan
to submit the thesis. A cloud repository link will be generated for you to upload the thesis.

Upload the thesis as a pdf to the cloud. This date is considered as the Date of Submission.

Send the following documents to Deans office by email.

* - An informal application letter for Doctoral Examination. Mention the thesis title
and thesis supervisors in the letter. Sign the letter.
* - [Confirmation of Reporting Obligation](Confirmation_of_Reporting_Obligation.pdf)
* - [Signed Declaration](Dissertation Versicherung PromO 2011.pdf)
* - [Proof of completion of course work](Proof_of_Taught_Courses.pdf)
* - Thesis title pages. Information in this [template](Dissertation Titelblatt PromO 2011.pdf) should be there. (Send the .tex file too.)
* - Certificate of semester enrollment. It can be downloaded from the RUB eCampus.

Please check the [leaflet](Leaflet3_AdmissionOfExamination.pdf) for the clarification.

After the Deans Office had done proofing the above documents send it by post to them.
Confirm the address with the office.
Send the original documents if it has your signature in it. Along with the above documents,
send
* - Three hard copies of Thesis.
* - A CD-ROM with Thesis pdf in it.

Now the two supervisors will send a report on your thesis. It may take 2-3 weeks.
Once it is done Deans office will inform you. The thesis will be displayed to the
Physics faculty for next two weeks. Meanwhile you can discuss with your two supervisors
to fix a date for the exam. This date should be at least two weeks after the end of display
period. Inform the Deans office about the dates you decided.

A letter with your thesis committee members names and the date of Disputation will be
issued by Deans office. If not please ask them.
Contact each member of your committee and fix a meeting with them individually.
This meeting will be good to introduce you and explain your project to them briefly.


## All the best for you exam.

After your defense, a certificate will be issued from Deans office stating that you
have passed the examination.

# Submit the Thesis to Library.

In order to obtain Doctoral certificate, the Thesis should be published at University
Library.

Confirm that the Date of Disputation is written on the second page of your thesis.

Use this link to complete this procedure
(https://hss-opus.ub.ruhr-uni-bochum.de/opus4/home/index/help)

After this contact at (theses-ub@rub.de) for further steps.
You may have to email the certificate you got from Deans Office to them.

After completion of these documents and sending three hard copies of thesis and other
documents to the library, they send a completion of deposit certificate to Deans Office.

Then contact Deans office to get the Doctoral Certificate.

Best Wishes.
